import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return ['Maria Curie', 'Mae Jemison', 'Adam Hoffmann'];
  }
});
